module;

#include <DotWriter.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Support/raw_ostream.h>

export module cfg;

using namespace llvm;
using DotWriter::Color;

constexpr std::array<DotWriter::Color::e, 5> kDfgColors = {Color::RED, Color::PURPLE, Color::BLUE,
                                                           Color::YELLOW, Color::ORANGE};

export namespace utils {
std::string ToString(const void *ptr) { return std::to_string(reinterpret_cast<uintptr_t>(ptr)); }
std::string ToPort(unsigned successorsNum, unsigned successorIdx) {
  if (successorsNum == 2) {
    if (successorIdx == 0) {
      return "t";
    }
    return "f";
  }
  return {};
}

} // namespace utils

export class ModuleCFG : public DotWriter::Graph {
public:
  ModuleCFG(DotWriter::IdManager *uniqId, std::string name)
      : DotWriter::Graph(uniqId, true, std::move(name)) {}

  void Print(std::ostream &out, unsigned tabDepth) override {
    out << "digraph " << GetId() << " {\n";
    PrintNECS(out, tabDepth);
    out << "}\n";
  }

  auto AddBB(DotWriter::Cluster *clusterPtr, const BasicBlock &BB) {
    auto bbStringPtr = utils::ToString(std::addressof(BB));

    auto node = clusterPtr->AddNode(dotLabel(BB), bbStringPtr, true);
    BBNodeMap[std::addressof(BB)] = node;
    return node;
  }

  auto AddEdge(const BasicBlock *src, const BasicBlock *dst) {
    return DotWriter::Graph::AddEdge(getNode(src), getNode(dst));
  }

  void dfgEdges(DotWriter::Cluster *cluster, const BasicBlock &BB) {
    size_t colorIdx = 0;
    for (auto &instr : BB) {
      for (const auto &user : instr.users()) {
        if (auto *userInstrPtr = dyn_cast<Instruction>(user)) {

          auto instrPtrS = utils::ToString(std::addressof(instr));
          auto userPtrS = utils::ToString(userInstrPtr);

          auto addEdge = [&](std::string style) {
            auto *edge = cluster->AddEdge(BBNodeMap[std::addressof(BB)],
                                          BBNodeMap[userInstrPtr->getParent()]);
            auto &attributes = edge->GetAttributes();

            attributes.SetStyle(std::move(style));
            attributes.SetHeadPort(userPtrS + ":w");
            attributes.SetTailPort(instrPtrS + ":w");
            attributes.SetColor(kDfgColors[colorIdx % kDfgColors.size()]);
          };

          addEdge("invis");
          addEdge("dashed");

          colorIdx += 1;
        }
      }
    }
  }

private:
  DotWriter::Node *getNode(const BasicBlock *BB) { return BBNodeMap[BB]; }

  std::string dotLabel(const BasicBlock &BB) {
    std::string dumpStr = "< <table style='rounded' cellborder='0'>";
    raw_string_ostream dump(dumpStr);

    auto row = [](auto&& value) constexpr {
      return "<tr>" + std::forward<std::string>(value) + "</tr>\n";
    };
  
    auto cell = [](auto&& value, auto&& attrs = "") constexpr {
      return "<td " + std::forward<std::string>(attrs) + ">" + std::forward<std::string>(value) + "</td>";
    };

    for (auto &instr : BB) {
      dumpStr += "<tr><td colspan='2' port='";
      dumpStr += utils::ToString(std::addressof(instr));
      dumpStr += "'>";
      instr.print(dump);
      dumpStr += "</td></tr>\n";
    }

    auto &lastInstr = BB.back();
    if (lastInstr.isTerminator()) {
      if (!dyn_cast<ReturnInst>(std::addressof(lastInstr))) {
        dumpStr += "<hr></hr>\n";
        switch (lastInstr.getNumSuccessors()) {
        case 1:
          dumpStr += row(cell("B", "colspan='2'"));
          break;
        case 2:
          dumpStr += row(cell("T", "port='t'") + "<vr></vr>" + cell("F", "port='f'"));
          break;
        default: 
          dumpStr += row(cell("unreacheable", "colspan='2'"));
        }
      }
    }
    dumpStr += "</table>>";

    return dumpStr;
  }

private:
  std::map<const BasicBlock *, DotWriter::Node *> BBNodeMap;
};
