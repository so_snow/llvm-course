#include <DotWriter.h>
#include <fstream>
#include <llvm/IR/Module.h>
#include <llvm/IR/PassManager.h>
#include <llvm/Passes/PassBuilder.h>
#include <llvm/Passes/PassPlugin.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/Utils/ModuleUtils.h>

import cfg;
using namespace llvm;

constexpr const char *kPassName = "myperf";
constexpr const char *kDumpFile = "dump.dot";
constexpr const char *kDumpWrapperFile = "dump.cpp";
constexpr const char *kDumpFunc = "dump";

namespace {

struct CfgBuilder : public PassInfoMixin<CfgBuilder> {
  PreservedAnalyses run(Module &M, ModuleAnalysisManager &MAM) {
    if (M.getName().ends_with(kDumpWrapperFile)) {
      return PreservedAnalyses::all();
    }

    DotWriter::IdManager manager;
    ModuleCFG graph(&manager, M.getName().str());

    graph.GetDefaultNodeAttributes().SetShape(DotWriter::NodeShape::PLAINTEXT);

    auto *moduleCluster = graph.AddCluster();
    for (const auto &func : M) {
      if (func.isDeclaration()) {
        continue;
      }

      auto *funcCluster = moduleCluster->AddCluster(func.getName().str());

      for (const auto &BB : func) {
        graph.AddBB(funcCluster, BB);
      }

      for (const auto &BB : func) {
        graph.dfgEdges(funcCluster, BB);

        auto &lastInstr = BB.back();
        if (lastInstr.isTerminator()) {

          for (int idx = 0; idx < lastInstr.getNumSuccessors(); ++idx) {

            auto *uses_bb = lastInstr.getSuccessor(idx);
            auto *edge = graph.AddEdge(std::addressof(BB), uses_bb);
            edge->GetAttributes().SetTailPort(utils::ToPort(lastInstr.getNumSuccessors(), idx));
          }
        }
      }
    }

    std::ofstream file(kDumpFile);
    graph.Print(file, 1);

    return PreservedAnalyses::all();
  }

  static bool isRequired() { return true; }
};

static Constant *CreateGlobalCounter(Module &M, StringRef GlobalVarName) {
  auto &CTX = M.getContext();

  Constant *newGlobalVar = M.getOrInsertGlobal(GlobalVarName, IntegerType::getInt32Ty(CTX));

  GlobalVariable *newGV = M.getNamedGlobal(GlobalVarName);
  newGV->setLinkage(GlobalValue::CommonLinkage);
  newGV->setAlignment(MaybeAlign(4));

  newGV->setInitializer(ConstantInt::get(CTX, APInt(32, 0)));

  return newGlobalVar;
}

struct BBInvokeCounter : public PassInfoMixin<BBInvokeCounter> {
  PreservedAnalyses run(Module &M, ModuleAnalysisManager &MAM) {

    if (M.getName().ends_with(kDumpWrapperFile)) {
      return PreservedAnalyses::all();
    }

    bool instrumented = false;

    StringMap<Constant *> CallCounterMap;
    StringMap<Constant *> BBNameMap;

    auto &CTX = M.getContext();
    for (auto &func : M) {
      if (func.isDeclaration()) {
        continue;
      }
      for (auto &BB : func) {
        IRBuilder<> Builder(&*BB.getFirstInsertionPt());

        auto counterName = utils::ToString(std::addressof(BB));

        auto *var = CreateGlobalCounter(M, counterName);
        CallCounterMap[counterName] = var;

        auto *BBName = Builder.CreateGlobalStringPtr(counterName);
        BBNameMap[counterName] = BBName;

        LoadInst *Load = Builder.CreateLoad(IntegerType::getInt32Ty(CTX), var);
        Value *Inc = Builder.CreateAdd(Builder.getInt32(1), Load);
        Builder.CreateStore(Inc, var);
      }

      instrumented = true;
    }
    if (false == instrumented)
      return PreservedAnalyses::all();

    PointerType *nameArgTy = PointerType::getUnqual(IntegerType::getInt8Ty(CTX));
    PointerType *countArgTy = PointerType::getUnqual(IntegerType::getInt32Ty(CTX));

    FunctionType *dumpTy = FunctionType::get(Type::getVoidTy(CTX), {nameArgTy, countArgTy}, false);
    FunctionCallee dump = M.getOrInsertFunction(kDumpFunc, dumpTy);

    Function *dumpF = dyn_cast<Function>(dump.getCallee());
    dumpF->setDoesNotThrow();

    FunctionType *dumpWrapperTy = FunctionType::get(Type::getVoidTy(CTX), {}, false);
    Function *dumpWrapperF =
        dyn_cast<Function>(M.getOrInsertFunction("dump_wrapper", dumpWrapperTy).getCallee());

    BasicBlock *retBlock = BasicBlock::Create(CTX, "enter", dumpWrapperF);
    IRBuilder<> Builder(retBlock);

    for (auto &[key, constant] : CallCounterMap) {
      auto *loadCounter = Builder.CreateLoad(IntegerType::getInt32Ty(CTX), constant);
      Builder.CreateCall(dump, {BBNameMap[key], loadCounter});
    }

    Builder.CreateRetVoid();
    appendToGlobalDtors(M, dumpWrapperF, 0);

    return PreservedAnalyses::none();
  }

  static bool isRequired() { return true; }
};
} // namespace

PassPluginLibraryInfo getPassPluginInfo() {
  const auto callback = [](PassBuilder &PB) {
    PB.registerOptimizerLastEPCallback([](ModulePassManager &MPM, OptimizationLevel) {
      MPM.addPass(CfgBuilder());
      MPM.addPass(BBInvokeCounter());
    });
  };

  return {LLVM_PLUGIN_API_VERSION, kPassName, LLVM_VERSION_STRING, callback};
};

extern "C" LLVM_ATTRIBUTE_WEAK PassPluginLibraryInfo llvmGetPassPluginInfo() {
  return getPassPluginInfo();
}
