#include <cstdint>
#include <fstream>

extern "C" void dump(const char *bb_name, uint64_t call_count) {
  std::ofstream dump("dump.cfg", std::ios_base::app);
  dump << bb_name << ": " << call_count << "\n";
}