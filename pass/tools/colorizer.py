import pydotplus, argparse
import html.parser
import configparser

legend_label = """<
    <TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0">
    <TR>
      <TD><FONT point-size="20"> data flow</FONT></TD>
      <TD><FONT point-size="20"> control flow</FONT></TD>
    </TR>
    <TR>
      <TD><FONT point-size="40">⟶</FONT></TD>
      <TD>
<FONT color="red" point-size="40">⤏<BR/></FONT>
<FONT color="blue" point-size="40">⤏<BR/></FONT>
<FONT color="purple" point-size="40">⤏<BR/></FONT>
      </TD>
    </TR>
    </TABLE>>
"""

class htmlTableParser(html.parser.HTMLParser):
    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.output = ""
        self.color = "1"
    
    def handle_starttag(self, tag, attrs):
        self.output += f"<{tag} "
        if tag == "table":
            attrs.append(("bgcolor", self.color))

        for key, value in attrs:
            self.output += f"{key}='{value}' "

        self.output += ">"

    def handle_endtag(self, tag):
        self.output += f"</{tag}>"

    def handle_data(self, data):
        self.output += data

    def get(self):
        output = self.output
        self.output = ""
        return output
    
    def set_fillcolor(self, color):
        self.color = color


def map_rdylbu11(current, min, max):
    scale = float(max - current) / float(max - min) * 8
    return str(int(scale) + 2)

def colorize(dot_file_path: str, stat: dict):
    parser = htmlTableParser()
    graph = pydotplus.graph_from_dot_file(dot_file_path)

    minimum = min(stat.values())
    maximum = max(stat.values())
    middle_count = sum(stat.values()) / len(stat) 

    graph.del_node("\"\\n\"")
    for main_cluster in graph.get_subgraphs():
        main_cluster.del_node("\"\\n\"")
        main_cluster.set_labeljust("l")
        main_cluster.set_label(legend_label)
    
        for cluster in main_cluster.get_subgraphs():
            cluster.del_node("\"\\n\"")
        
            for node in cluster.get_nodes():
                node.set_colorscheme("rdylbu11")
            
                parser.set_fillcolor(map_rdylbu11(stat[node.get_name()], minimum, maximum))
            
                parser.feed(node.get_label())
                node.set_label(parser.get())

    graph.write_png("output.png")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="dot_colorizer")
    parser.add_argument("--dot", type=str, dest="dot_file", help="dot file path")
    parser.add_argument("--cfg", type=str, dest="cfg_file", help="path to cfg dump")

    args = parser.parse_args()

    # config format: bb_[0-9]*: <value>
    config = configparser.ConfigParser()


    with open(args.cfg_file) as cfg:
        config.read_string("[default]\n" + cfg.read())

    raw_stat = dict(config["default"])
    stat = {}
    for key in raw_stat:
        stat[key] = int(raw_stat[key])

    print(stat)
    colorize(args.dot_file, stat)
