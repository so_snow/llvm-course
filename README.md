# llvm-course




## Usage 
### Requirements
* `ninja` 1.11 or newer
* `cmake` 3.28 or newer
* `llvm/clang 16.0` and newer
* `pydotplus`

### Build pass
    cd pass
    cmake -DCMAKE_CXX_COMPILER=clang++-18 -G Ninja -S . -B build
    cmake --build build

It will generate `libperf_pass.so`

### Invoke pass
    clang++-18 -fpass-plugin=./build/src/libperf_pass.so src/dump.cpp <input files> -o <instrumented file>

It will generate `dump.dot` with cfg and dfg of program
### Colorize graph
    <run instrumented file>
    python3 tools/colorizer.py --dot dump.dot --cfg dump.cfg 

### Examples
#### Quite simple program
```cpp
#include <cstdio>

int foo(int a, int b) {
  int c = a + b;
  if (c > 10) {
    return c - 10;
  } else if (c & 10) {
    while (c > 0) {
      c -= 5;
      return c;
    }
  }
  return c * a;
}


int bar(int a, int b) {
  if (a == 100) {
    return 20 + b;
  }
  return a > b ? 10 : b;
}

int main() {
  int a = 0, b = 0;
  std::scanf("%d %d", &a, &b);
  std::printf("%d", foo(a, b));
  std::printf("%d", bar(a, b));
  return 0;
}
```

<img align="center" src="img/simple.png">

#### Another simple example
```cpp
#include <cstdio>

int cold(int num) { return num; }

int hot(int num) {
  int count = 0;
  for (int i = 0; i < num; ++i) {
    count += 1;
  }
  return count;
}

int main() {
  int num = 0;
  std::scanf("%d", &num);
  hot(num);
  cold(num);
  return 0;
}
```

<img align="center" src="img/simple2.png">

#### Complex example 
##### Invoke 
    clang++-18 -O3-fpass-plugin=.libperf_pass.so src/dump.cpp -xc cat.c -o cat
    ./cat cat

Source located in [play/cat](play/cat/cat.c)

<img align="center" src="img/cat.png">
