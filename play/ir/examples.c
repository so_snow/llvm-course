// trunc
int trunc_instr(long val) { return val; }

// getelementptr
[[clang::optnone]] int getelementptr() {
  struct S {
    int val;
  };
  struct S s = {1};
  return s.val;
}

// srem
int srem(int val) { return val % 10; }

// lshr
unsigned lshr(unsigned val) { return val >> 1; }

// zext
int zext(unsigned short val) { return val; }

// bitcast
int *bitcast(long *val) { return (int *)val; }

// sitofp
float sitofp(int val) { return val; }

// select
float select(float a, float b) {
  if (a == b)
    return 1;
  else
    return 0;
}

// fpext
double fpext(float val) { return val; }

// tail call
int tail_call(int n, int div) {
  if (n <= 1) {
    return 1;
  }
  return n * tail_call(n - 1, div) % div;
}
